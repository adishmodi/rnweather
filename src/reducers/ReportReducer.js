import { FIND_WEATHER_REPORT } from '../actions/types';

const INITIAL_STATE ={};

export default (  state = INITIAL_STATE , action ) =>{
  switch (action.type) {
    case FIND_WEATHER_REPORT:
      return action.payload;
    default:
      return state;
  }
}
