import { ZIPCODE_CHANGED } from '../actions/types';

const INITIAL_STATE = {
  zipcode: '',
};

export default (state = INITIAL_STATE, action) =>{
  switch (action.type) {
    case ZIPCODE_CHANGED:
      return {...state, zipcode:action.payload };
    default:
      return state;
  };
};
