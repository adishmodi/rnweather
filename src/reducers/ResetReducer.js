import { RESET_HANDLER } from '../actions/types';

const INITIAL_STATE ={reset: ''};

export default (  state = INITIAL_STATE , action ) =>{
  switch (action.type) {
    case RESET_HANDLER:
      return action.payload;
    default:
      return state;
  }
}
