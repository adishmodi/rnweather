import React from 'react';
import { Scene, Router, Actions } from 'react-native-router-flux';
import LandingScreen from './components/LandingScreen';
import WeatherReport from './components/WeatherReportFlatList';
import SampleOffline from './components/SampleOffline';
import Sample from './components/Sample';

const RouterComponent = () => {
  return (
    <Router >
      <Scene key="root">
        <Scene key="Main" component={LandingScreen}  title="Landing" hideNavBar initial />
        <Scene key="weatherreport" component={WeatherReport} title="Weather Report" />
        <Scene key="SampleOffline" component={SampleOffline} title="SampleOffline"  />
        <Scene key="Sample" component={Sample} title="Sample"  />
      </Scene>
    </Router>
  );
};

export default  RouterComponent;
