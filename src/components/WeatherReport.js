import React, { Component } from 'react';
import { connect } from 'react-redux';
import {Image, Keyboard,StyleSheet, Text, View, TextInput, TouchableOpacity, SectionList , TouchableHighlight} from 'react-native';
import { zipcodechanged , weatherforecast} from '../actions';
import {ListContentText , ListHeaderText, Button,OfflineNotice } from  './common';
/*import { Select, Option } from 'react-native-select-list';*/


class WeatherReport extends Component
{
  onZipcodeChanged(text){
    this.props.zipcodechanged(text);
  }

  onButtonPress() {
    Keyboard.dismiss();
    const { zipcode } = this.props;
    this.props.weatherforecast({ zipcode });
  }

  ResetScreen(){
    Keyboard.dismiss();
    this.props.zipcodechanged('');
    this.props.weatherforecast('')
  }

  renderWeatherCityInfo()
  {
    if(this.props.city){
      const{name,country} = this.props.city;
        if(name){
          return(
            <View style={{ flexDirection: 'row' , justifyContent: 'center', alignItems:'center',  }}>
              <Text style={{fontSize:20, fontWeight:'bold'}}>City : {name}</Text>
              <Text style={{fontSize:20,marginLeft:20, fontWeight:'bold'}}>Country: {country}</Text>
            </View>
          );
      }
    }
  }

  /*renderSelectionList(){
    return(
      <View>
        <Select>
          <Option value='1'>List item 1</Option>
          <Option value='2'>List item 2</Option>
          <Option value='3'>List item 3</Option>
        </Select>
      </View>
    );
  }
*/

renderHeader(){
  return(
    <View style={styles.flatlistContainer}>
      <ListHeaderText headerText='Date Time'></ListHeaderText>
      <ListHeaderText headerText='Min Temp.'></ListHeaderText>
      <ListHeaderText headerText='Max Temp.'></ListHeaderText>
      <ListHeaderText headerText='Humidity'></ListHeaderText>
      <ListHeaderText headerText=''></ListHeaderText>
    </View>
  );
}

renderListItem = ({item}) =>(
  <View style={styles.flatlistContainer}>
    <ListContentText contentText={item.dt_txt}></ListContentText>
    <ListContentText contentText={item.main.temp_min}></ListContentText>
    <ListContentText contentText={item.main.temp_max}></ListContentText>
    <ListContentText contentText={item.main.humidity}></ListContentText>
    {/*<Text style={styles.flatTextContent}> {item.weather.map((info) => info.description)} </Text>*/}
    <Image
      style={{width: 50, height: 50, flex:.2,}}
      source={{uri: 'http://openweathermap.org/img/w/' + item.weather.map((info) => info.icon) + '.png' }}
    />
  </View>
);

_renderSectionHeader = ({section}) => {
     return (
       <View style={styles.sectionHeader}>
         <Text style={styles.header}>{section.key}</Text>
       </View>
     )
 }

renderWeatherInfoInList(){
  if (this.props.list) {
    return(
      <SectionList
        renderItem={this.renderListItem}
        renderSectionHeader={({section: {title}}) => (
          <Text style={{fontWeight: 'bold'}}>{title}</Text>
        )}
        sections={this.props.list}
        />
    )
  }
}


  render() {
    return(
      <View>
      <OfflineNotice />
        <View style={{ flexDirection: 'row' ,padding:20}} >
          <TextInput
                    style={styles.textinputStyle}
                    keyboardType='number-pad'
                    placeholder='Enter zipcode'
                    onChangeText={this.onZipcodeChanged.bind(this)}
                    value={this.props.zipcode}
                     ref={zipcode => { this.textInputRef = zipcode; }}
                    />

          <Button  title='Show Report' onPress={this.onButtonPress.bind(this)} >Forecast</Button>
          <Button title='Reset' onPress={this.ResetScreen.bind(this)}>Reset</Button>

        </View>

        {this.renderWeatherCityInfo()}
        <View style={{marginTop:20}}>
          {this.renderWeatherInfoInList()}
        </View>
      </View>
    )
  }
}


const mapStateToProps = (info) => {
  const { zipcode, error, loading } = info.generic;
  const { city,list } = info.report;
  return { zipcode, error, loading, city ,list};
};

export default connect(mapStateToProps, {
  zipcodechanged , weatherforecast
})(WeatherReport);

const styles =  StyleSheet.create ({
  textinputStyle:{
    height:40,
    borderColor:'grey',
    borderWidth:1,
    width:200,
    fontSize: 18,
    padding:5
  },
flatlistContainer:{
   flexDirection: 'row' ,
   borderWidth:1,
   borderColor:'black',
   justifyContent: 'center',
   alignItems:'center',
 },
});
