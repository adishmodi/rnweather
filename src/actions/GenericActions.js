import {ZIPCODE_CHANGED , RESET_HANDLER } from './types';


export const zipcodechanged = (text) => {
  return {
    type : ZIPCODE_CHANGED,
    payload: text
  };
};

export const resethandler = (text) => {
  return {
    type : RESET_HANDLER,
    payload: text
  };
};
