import React, { Component } from 'react';
import {View,Text} from  'react-native';
import {Button, OfflineNotice } from './common';
import BlinkView from 'react-native-blink-view';
import { Actions  } from 'react-native-router-flux';

class LandingScreen extends Component
{
  RenderBlink(){
      return(
        <BlinkView delay={1000}>
          <Text style={styles.welcome}>Depeloped by Adish Jain !!</Text>
        </BlinkView>
      )
  }
  render(){

    const goToWeatherReport = () => {
        Actions.weatherreport()
     };

    //const{ navigate } = this.props.navigation;
    const welcomeMsg='Welcome to Weather App !!';

    return(
      <View style={{marginTop:20,flex:1}}>

        <View style={{flexDirection: 'row',justifyContent: 'center', alignItems:'center', }}>
          <Text style={{ fontSize:18, margin:20 }}> {welcomeMsg}</Text>
        </View>
        <Button onPress={goToWeatherReport} >Click Here For Weather Report !!</Button>
        <View style={styles.footer} >
          <this.RenderBlink />
         </View>
      </View>
    )}
}

const styles = {
  welcome: {
   fontSize: 16,
   textAlign: 'center',
   margin: 10,
 },
 footer: {
  width: '100%',
  height: 50,
  backgroundColor: '#FF9800',
  justifyContent: 'center',
  alignItems: 'center',
  position: 'absolute',
  bottom: 0
 }
}

export default LandingScreen;
