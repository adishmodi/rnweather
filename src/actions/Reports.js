import axios from 'axios';
import {FIND_WEATHER_REPORT} from './types';

const BaseAPIUrl='https://samples.openweathermap.org/data/2.5/forecast'
const AppId='appid=b6907d289e10d714a6e88b30761fae22';

export const weatherforecast = ({zipcode}) => {
  //https://samples.openweathermap.org/data/2.5/forecast?zip={zipcode},ind&appid=b6907d289e10d714a6e88b30761fae22'

  const apiurl=BaseAPIUrl  +'?zip='+zipcode + ',ind&'+AppId;

  const request = axios.get(apiurl);
  console.log(apiurl);

  return (dispatch) => {
         request.then(res => {
             dispatch({
               type: FIND_WEATHER_REPORT ,
               payload : res.data
             });
         });
  };
};
