export const ZIPCODE_CHANGED = 'zipcode_changed';

export const FIND_WEATHER_REPORT = 'find_weather_report';

export const RESET_HANDLER = 'reset';
