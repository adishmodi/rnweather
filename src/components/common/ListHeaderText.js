import React from 'react';
import { Text } from 'react-native';

const ListHeaderText = (props) =>{

  return  (
      <Text style={styles.flatlistHeaderTextContent}>{props.headerText}</Text>
  );
}

const styles = {
  flatlistHeaderTextContent:{
    flex:.2,
    fontWeight: 'bold',
    fontSize: 18,
    textAlign:'center'
  }
}

export {ListHeaderText};
