import { combineReducers } from 'redux';
import GenericReducer from './GenericReducer';
import ReportReducer from './ReportReducer';
import ResetReducer from './ResetReducer';

export default combineReducers({
  generic : GenericReducer,
  report : ReportReducer,
  reset: ResetReducer
});
