import React from 'react';
import  Sample  from '../src/components/Sample';
import renderer from 'react-test-renderer';

test('renders correctly', () => {
  const tree = renderer.create(<Sample />).toJSON();
  expect(tree).toMatchSnapshot();
});
