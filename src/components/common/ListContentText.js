import React from 'react';
import { Text } from 'react-native';

const ListContentText = (props) =>{

  return  (
      <Text style={styles.flatTextContent}>{props.contentText}</Text>
  );
}

const styles = {
  flatTextContent:
  {
    flex:.2,
    textAlign:'center'
  },
}

export {ListContentText};
