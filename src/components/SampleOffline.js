import React, { Component } from 'react';
import {View,Text} from  'react-native';
import { OfflineNotice } from './common';


class SampleOffline extends Component{
    render() {
      return (
        <View >
          <OfflineNotice />
          <Text >Welcome to React Native!</Text>
        </View>
      );
    }
}

export default SampleOffline;
