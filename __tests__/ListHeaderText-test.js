import React from 'react';
import  {ListHeaderText}  from '../src/components/common';
import renderer from 'react-test-renderer';

test('renders correctly', () => {
  const tree = renderer.create(<ListHeaderText />).toJSON();
  expect(tree).toMatchSnapshot();
});
