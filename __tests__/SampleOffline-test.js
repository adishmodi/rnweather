import React from 'react';
import SampleOffline  from '../src/components/SampleOffline';
import renderer from 'react-test-renderer';

test('renders correctly', () => {
  const tree = renderer.create(<SampleOffline />).toJSON();
  expect(tree).toMatchSnapshot();
});
