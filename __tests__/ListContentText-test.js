import React from 'react';
import  {ListContentText}  from '../src/components/common';
import renderer from 'react-test-renderer';

test('renders correctly', () => {
  const tree = renderer.create(<ListContentText />).toJSON();
  expect(tree).toMatchSnapshot();
});
