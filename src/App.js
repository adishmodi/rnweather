import React, { Component } from 'react';
import { Provider } from 'react-redux';
import { createStore,compose, applyMiddleware} from 'redux';
import ReduxThunk from 'redux-thunk';
import reducers from './reducers';
import RouterComponent from './RouterComponent';
import WeatherReport from './components/WeatherReport';
import { persistStore, persistReducer } from 'redux-persist';
import { AsyncStorage } from 'react-native'

class App extends Component {

  render(){
     const store = createStore(
        reducers,
        {},
        compose(
          applyMiddleware(ReduxThunk)
        )
      );

     return (
     <Provider store={store}>
       <RouterComponent />
     </Provider>
   );
 };
};

//persistStore(App, {storage: AsyncStorage, whitelist:['abcd']});
export default App;
