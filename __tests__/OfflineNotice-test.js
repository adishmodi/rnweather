import React from 'react';
import  {OfflineNotice}  from '../src/components/common';
import renderer from 'react-test-renderer';

test('renders correctly', () => {
  const tree = renderer.create(<OfflineNotice />).toJSON();
  expect(tree).toMatchSnapshot();
});
